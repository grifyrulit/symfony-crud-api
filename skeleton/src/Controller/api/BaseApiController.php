<?php


namespace App\Controller\api;

use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class BaseApiController extends AbstractController
{
    public EntityManagerInterface $entityManager;
    public ParameterBagInterface $parameters;
    public Request $request;
    public $container;
    public LoggerInterface $logger;
    public SerializerInterface $serializer;
    public UserPasswordEncoderInterface $passwordEncoder;
    public ValidatorInterface $validator;

    public function __construct(EntityManagerInterface $entityManager,
                                ContainerInterface $container,
                                ParameterBagInterface $parameters, LoggerInterface $logger, UserPasswordEncoderInterface $passwordEncoder, ValidatorInterface $validator)
    {
        $this->request = Request::createFromGlobals();
        $this->container = $container;
        $this->parameters = $parameters;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->serializer = SerializerBuilder::create()->build();
        $this->passwordEncoder = $passwordEncoder;
        $this->validator = $validator;
    }

    public function createApiResponse($data, $statusCode = 200)
    {
        return new Response($this->serializer->serialize($data, 'json'), $statusCode);
    }
}