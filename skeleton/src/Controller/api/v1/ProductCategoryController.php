<?php


namespace App\Controller\api\v1;

use App\Controller\api\BaseApiController;
use App\Entity\ProductCategory;
use App\Form\ProductCategoryType;
use App\Service\EntityService\ProductCategoryService\{CreateProductCategory,
    DeleteProductCategory,
    GetProductCategories,
    UpdateProductCategory};
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/** @Route("api/v1/category/", name="api_v1_category_") */
class ProductCategoryController extends BaseApiController
{
    /**
     * @Route("list/", name="list", methods={"GET"})
     *
     * @param GetProductCategories $getProductCategories
     * @return Response
     */
    public function listAction(GetProductCategories $getProductCategories)
    {
        return $this->createApiResponse($getProductCategories->call());
    }

    /**
     * @Route("add/", name="add", methods={"POST"})
     *
     * @param CreateProductCategory $createProductCategory
     * @return Response
     */
    public function addAction(CreateProductCategory $createProductCategory)
    {
        [$message, $code] = $createProductCategory->call($this->request, $this->createForm(ProductCategoryType::class, new ProductCategory()));

        return $this->createApiResponse($message, $code);
    }

    /**
     * @Route("update/{id}", name="update", methods={"PATCH"})
     * @param ProductCategory $category
     * @param UpdateProductCategory $updateProductCategory
     * @return Response
     */
    public function updateAction(ProductCategory $category, UpdateProductCategory $updateProductCategory)
    {
        [$message, $code] = $updateProductCategory->call($this->request, $this->createForm(ProductCategoryType::class, $category));

        return $this->createApiResponse($message, $code);
    }

    /**
     * @Route("delete/{id}", name="delete", methods={"DELETE"})
     * @param ProductCategory $category
     * @param DeleteProductCategory $deleteProductCategory
     * @return Response
     */
    public function deleteAction(ProductCategory $category, DeleteProductCategory $deleteProductCategory)
    {
        [$message, $code] = $deleteProductCategory->call($category);

        return $this->createApiResponse($message, $code);
    }
}