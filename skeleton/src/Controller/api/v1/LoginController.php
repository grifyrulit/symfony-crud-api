<?php


namespace App\Controller\api\v1;

use App\Controller\api\BaseApiController;
use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/** @Route("api/v1/", name="api_v1_") */
class LoginController extends BaseApiController
{
    /**
     * @Route("register/", name="register", methods={"POST"})
     *
     * @return Response
     */
    public function register()
    {
        $userData = json_decode($this->request->getContent(), true);
        $username = isset($userData['username']) && !empty($userData['username']) ? $userData['username'] : null;
        $password = isset($userData['password']) && !empty($userData['password']) ? $userData['password'] : null;

        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'username' => $username,
        ]);

        if (!is_null($user)) {
            return $this->createApiResponse(['errors' => ['message' => 'User already exist']], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user = new User();

        $user->setUsername($username)
            ->setPassword($password);

        $errors = $this->validator->validate($user);
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $output['errors'][] = [
                    'propertyPath' => $error->getPropertyPath(),
                    'message' => $error->getMessage(),
                    'value' => $error->getInvalidValue(),
                ];
            }
            return $this->createApiResponse($output, Response::HTTP_BAD_REQUEST);
        }
        $user->setPassword(
            $this->passwordEncoder->encodePassword($user, $password)
        );
        $errors = $this->validator->validate($user);
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $output['errors'][] = [
                    'propertyPath' => $error->getPropertyPath(),
                    'message' => $error->getMessage(),
                    'value' => $error->getInvalidValue(),
                ];
            }
            return $this->createApiResponse($output, Response::HTTP_BAD_REQUEST);
        }
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $this->createApiResponse($user, Response::HTTP_CREATED);
    }

    /**
     * @Route("login/", name="login", methods={"POST"})
     *
     * @return Response
     */
    public function login(JWTTokenManagerInterface $JWTManager)
    {
        $userData = json_decode($this->request->getContent(), true);
        $username = isset($userData['username']) && !empty($userData['username']) ? $userData['username'] : null;
        $password = isset($userData['password']) && !empty($userData['password']) ? $userData['password'] : null;

        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'username' => $username,
        ]);
        if (is_null($user)) {
            return $this->createApiResponse(['errors' => ['message' => 'Неверный логин или пароль']], Response::HTTP_FORBIDDEN);
        }
        $isValid = $this->passwordEncoder
            ->isPasswordValid($user, $password);

        if (!$isValid) {
            return $this->createApiResponse(['errors' => ['message' => 'Неверный логин или пароль']], Response::HTTP_FORBIDDEN);
        }
        return $this->createApiResponse(['token' => $JWTManager->create($user)], Response::HTTP_OK);

    }
}