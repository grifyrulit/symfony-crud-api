<?php


namespace App\Controller\api\v1;

use App\Controller\api\BaseApiController;
use App\Entity\Product;
use App\Form\ProductType;
use App\Service\EntityService\ProductService\{CreateProduct,
    DeleteProduct,
    GetProducts,
    UpdateProduct};
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/** @Route("api/v1/product/", name="api_v1_pdoruct_") */
class ProductController extends BaseApiController
{
    /**
     * @Route("list/", name="list", methods={"GET"})
     *
     * @param GetProducts $getProducts
     * @return Response
     */
    public function listAction(GetProducts $getProducts)
    {
        return $this->createApiResponse($getProducts->call($this->request));
    }

    /**
     * @Route("add/", name="add", methods={"POST"})
     *
     * @param CreateProduct $createProduct
     * @return Response
     */
    public function addAction(CreateProduct $createProduct)
    {
        [$message, $code] = $createProduct->call($this->request, $this->createForm(ProductType::class, new Product()));

        return $this->createApiResponse($message, $code);
    }

    /**
     * @Route("update/{id}", name="update", methods={"PATCH"})
     * @param Product $product
     * @param UpdateProduct $updateProduct
     * @return Response
     */
    public function updateAction(Product $product, UpdateProduct $updateProduct)
    {
        [$message, $code] = $updateProduct->call($this->request, $this->createForm(ProductType::class, $product));

        return $this->createApiResponse($message, $code);
    }

    /**
     * @Route("delete/{id}", name="delete", methods={"DELETE"})
     * @param Product $product
     * @param DeleteProduct $deleteProduct
     * @return Response
     */
    public function deleteAction(Product $product, DeleteProduct $deleteProduct)
    {
        [$message, $code] = $deleteProduct->call($product);

        return $this->createApiResponse($message, $code);
    }
}