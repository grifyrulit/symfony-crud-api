<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\ProductCategory;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductType extends AbstractType
{
    const TITLE_MAXLENGTH = 100;
    const DESCRIPTION_MAXLENGTH = 255;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Length([
                        'max' => self::TITLE_MAXLENGTH,
                    ]),
                    new NotBlank()
                ]
            ])
            ->add('description', TextareaType::class, [
                'required' => true,
                'constraints' => [
                    new Length([
                        'max' => self::DESCRIPTION_MAXLENGTH,
                    ]),
                    new NotBlank()
                ]
            ])
            ->add('price', NumberType::class, [
                'required' => true,
            ])
            ->add('categories', EntityType::class, [
                'class' => ProductCategory::class,
                'multiple' => true,
                'choice_value' => function (ProductCategory $category = null) {
                    return $category ? $category->getId() : '';
                },
                'required' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
