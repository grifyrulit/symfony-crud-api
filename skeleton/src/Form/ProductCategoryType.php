<?php

namespace App\Form;

use App\Entity\ProductCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductCategoryType extends AbstractType
{
    const TITLE_MAXLENGTH = 100;
    const SLUG_MAXLENGTH = 100;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Length([
                        'max' => self::TITLE_MAXLENGTH,
                    ]),
                    new NotBlank()
                ]
            ])
            ->add('slug', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Length([
                        'max' => self::SLUG_MAXLENGTH,
                    ]),
                    new NotBlank()
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductCategory::class,
        ]);
    }
}
