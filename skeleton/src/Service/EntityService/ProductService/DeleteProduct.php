<?php


namespace App\Service\EntityService\ProductService;


use App\Entity\Product;
use App\Service\EntityService\EntityService;

class DeleteProduct extends EntityService
{
    public function call(Product $product)
    {
        return $this->deleteEntity($product);
    }
}