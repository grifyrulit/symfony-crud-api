<?php


namespace App\Service\EntityService\ProductService;


use App\Entity\Product;
use App\Service\EntityService\EntityService;
use Symfony\Component\HttpFoundation\Request;

class GetProducts extends EntityService
{
    public function call(Request $request)
    {
        return $this->entityManager->getRepository(Product::class)->getByCategory($request->query->get('category'));
    }
}