<?php


namespace App\Service\EntityService\ProductService;


use App\Service\EntityService\EntityService;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class UpdateProduct extends EntityService
{
    public function call(Request $request, FormInterface $form)
    {
        return $this->updateEntity($request, $form);
    }
}