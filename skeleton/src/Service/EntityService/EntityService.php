<?php


namespace App\Service\EntityService;


use App\Entity\EntityInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class EntityService
{
    const SUCCESS = ['message' => true];
    const ERROR = ['message' => false];

    public EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createEntity(Request $request, FormInterface $form)
    {
        $entityData = $this->prepareData($request->getContent());

        $form->submit($entityData);
        if(!$form->isValid()){
            return [$this->getErrorsFromForm($form), 500];
        }

        try{
            $this->entityManager->persist($form->getData());
            $this->entityManager->flush();
        } catch(\Exception $exception) {
            return [self::ERROR, 500];
        }

        return [self::SUCCESS, 201];
    }

    public function updateEntity(Request $request, FormInterface $form)
    {
        $categoryData = $this->prepareData($request->getContent());

        $form->submit($categoryData);
        if(!$form->isValid()){
            return [$this->getErrorsFromForm($form), 500];
        }

        try{
            $this->entityManager->flush();
        } catch(\Exception $exception) {
            return [self::ERROR, 500];
        }

        return [self::SUCCESS, 200];
    }

    public function deleteEntity(EntityInterface $entity)
    {
        try{
            $this->entityManager->remove($entity);
            $this->entityManager->flush();
        } catch(\Exception $exception) {
            return [self::ERROR, 500];
        }

        return [self::SUCCESS, 200];
    }

    protected function prepareData(string $jsonString): array
    {
        $data = json_decode($jsonString, true);
        if (JSON_ERROR_NONE != json_last_error()) {
            throw new \Error("Invalid JSON format sent");
        }

        return $data;
    }

    protected function getErrorsFromForm(FormInterface $form): array
    {
        $errors = [];
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }
}