<?php


namespace App\Service\EntityService\ProductCategoryService;


use App\Entity\ProductCategory;
use App\Service\EntityService\EntityService;

class GetProductCategories extends EntityService
{
    public function call()
    {
        return $this->entityManager->getRepository(ProductCategory::class)->findAll();
    }
}