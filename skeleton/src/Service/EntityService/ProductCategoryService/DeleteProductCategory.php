<?php


namespace App\Service\EntityService\ProductCategoryService;


use App\Entity\ProductCategory;
use App\Service\EntityService\EntityService;

class DeleteProductCategory extends EntityService
{
    public function call(ProductCategory $productCategory)
    {
        return $this->deleteEntity($productCategory);
    }
}