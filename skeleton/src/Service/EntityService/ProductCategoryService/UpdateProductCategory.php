<?php


namespace App\Service\EntityService\ProductCategoryService;


use App\Service\EntityService\EntityService;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class UpdateProductCategory extends EntityService
{
    public function call(Request $request, FormInterface $form)
    {
        return $this->updateEntity($request, $form);
    }
}