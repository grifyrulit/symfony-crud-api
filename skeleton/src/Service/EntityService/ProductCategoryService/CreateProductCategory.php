<?php


namespace App\Service\EntityService\ProductCategoryService;


use App\Service\EntityService\EntityService;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class CreateProductCategory extends EntityService
{
    public function call(Request $request, FormInterface $form)
    {
        return $this->createEntity($request, $form);
    }
}