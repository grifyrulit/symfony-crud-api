<?php


namespace App\DataFixtures;


use App\Entity\ProductCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;

class ProductCategoryFixtures extends Fixture
{
    const CATEGORIES = [
        'podarki' => 'Подарки',
        'elektronika' => 'Электроника',
        'test' => 'Тест',
    ];

    public EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function load(ObjectManager $manager)
    {
        return $this
            ->addProductCategories();
    }

    private function addProductCategories()
    {
        foreach (self::CATEGORIES as $slug => $title) {
            $category = new ProductCategory();
            $category
                ->setTitle($title)
                ->setSlug($slug);
            $this->entityManager->persist($category);
        }

        $this->entityManager->flush();

        return $this;
    }
}