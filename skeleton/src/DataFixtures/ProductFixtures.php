<?php


namespace App\DataFixtures;


use App\Entity\Product;
use App\Entity\ProductCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    const PRODUCT_COUNT = 10;

    public EntityManagerInterface $entityManager;
    public Faker\Generator $faker;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->faker = Faker\Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        return $this
            ->addProduct()
            ->relateProductAndCategories();
    }

    private function addProduct()
    {
        for ($i = 0; $i < self::PRODUCT_COUNT; $i++) {
            $product = new Product();
            $product->setTitle($this->faker->word)
                ->setDescription($this->faker->sentence($nbWords = 6, $variableNbWords = true))
                ->setPrice($this->faker->randomNumber(2));
            $this->entityManager->persist($product);
        }
        $this->entityManager->flush();

        return $this;
    }

    private function relateProductAndCategories()
    {
        $products = $this->entityManager->getRepository(Product::class)->findAll();

        /** @var Product $product */
        foreach ($products as $product) {
            $productCategories = $this->entityManager->getRepository(ProductCategory::class)
                ->createQueryBuilder('q')
                ->addSelect('RAND() as HIDDEN rand')
                ->addOrderBy('rand')
                ->setMaxResults(rand(1, 3))
                ->getQuery()
                ->getResult();

            /** @var ProductCategory $category */
            foreach ($productCategories as $category) {
                $product->addCategory($category);
            }
        }

        $this->entityManager->flush();

        return $this;
    }

    public function getDependencies()
    {
        return array(
            ProductCategoryFixtures::class,
        );
    }
}