### Требования к системе

- Docker 19.03 или выше
- Docker-compose 1.17 или выше
- Git

### Запуск проекта
1) git clone <repository:url>
1) cd project_dir/
1) sudo make build-app

#### Команды для быстрого запуска из консоли
|  Команда | Описание  |
|---|---|
| `make start`          | Запуск контейнеров Docker           |
| `make start-build`    | Запуск сборки контейнеров Docker           |
| `make stop`           | Остановка контейнеров Docker              |
| `make restart`        | Перезапуск контейнеров Docker           |
| `make bash`           | Запуск bash-сессии для контейнера test-app (контейнер с веб-приложением)  |
| `make ps`             | Вывести список всех контейнеров |
| `make cache-clear`    | Очистка кэша приложения |
| `make run-server`     | Запуск веб-сервера приложения |
| `make build-app`      | Запуск скрипта сборка и запуска приложения |