FROM php:7.4-apache

# Install package dependencies
RUN apt-get update && apt-get install -y wget git unzip gnupg2

RUN apt-get update

ADD docker/php.ini /usr/local/etc/php/php.ini

RUN wget https://getcomposer.org/installer -O - -q \
    | php -- --install-dir=/bin --filename=composer --quiet

RUN apt-get update && apt-get install --no-install-recommends -y \
    libzip-dev \
    zlibc \
    zlib1g \
    && docker-php-ext-configure zip  \
    && docker-php-ext-install zip

RUN apt-get install -y \
    libpq-dev \
    libmemcached-dev \
    curl \
    git

# Enable default PHP extensions
RUN docker-php-ext-install mysqli pdo_mysql

# Set working directory
WORKDIR /var/www/html