start:
	docker-compose up -d

start-build:
	docker-compose up -d --build

stop:
	docker-compose down

restart:
	docker-compose restart

bash:
	docker-compose exec test-app bash

ps:
	docker ps

cache-clear:
	docker-compose exec test-app bin/console cache:clear

run-server:
	docker-compose exec test-app bin/console server:run 0.0.0.0:8089

build-app:
	@bash ./app.sh