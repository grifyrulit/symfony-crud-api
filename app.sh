#!/usr/bin/env bash

echo "Start building app"
cp skeleton/.env.local.dist skeleton/.env.local

echo "Building docker container"
make start-build

echo "Install composer dependencies"
docker-compose exec test-app composer install
docker-compose exec test-app composer update


echo "Creating database"
docker-compose exec test-app bin/console doctrine:database:create
docker-compose exec test-app bin/console doctrine:schema:update --force

echo "Load fixtures"
docker-compose exec test-app bin/console doctrine:fixtures:load --append

echo "Run server"
make run-server

echo "Build was finished successfully"
exit